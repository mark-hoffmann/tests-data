import os
import random
import typing

from d3m import container, exceptions, utils
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces.base import CallResult
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

__all__ = ('RandomClassifierPrimitive',)

Inputs = typing.Union[container.DataFrame, container.List, container.ndarray]
# TODO: This does not really capture that this primitive returns the same type as input.
#       This requires all other primitives following this primitive to accept and return an Union as well.
#       See: https://github.com/python/mypy/issues/6746
Outputs = typing.Union[container.DataFrame, container.List, container.ndarray]


class Params(params.Params):
    classes: typing.Optional[typing.List[typing.Any]]


class Hyperparams(hyperparams.Hyperparams):
    pass


class RandomClassifierPrimitive(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A primitive randomly classify a class. For test purposes.

    It uses the first column of ``outputs`` as a target column.
    """

    metadata = metadata_base.PrimitiveMetadata({
        'id': 'b8d0d982-fc53-4a3f-8a8c-a284fdd45bfd',
        'version': '0.1.0',
        'name': "Random Classifier",
        'python_path': 'd3m.primitives.classification.random_classifier.Test',
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/tests-data.git@{git_commit}#egg=test_primitives&subdirectory=primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.BINARY_CLASSIFICATION,
            metadata_base.PrimitiveAlgorithmType.MULTICLASS_CLASSIFICATION
        ],
        'primitive_family': metadata_base.PrimitiveFamily.CLASSIFICATION,
        'source': {
            'name': 'test',
            'contact': 'mailto:author@example.com',
            'uris': [
                # Unstructured URIs. Link to file and link to repo in this case.
                'https://gitlab.com/datadrivendiscovery/tests-data/blob/master/primitives/test_primitives/random_classifier.py',
                'https://gitlab.com/datadrivendiscovery/tests-data.git',
            ],
        },
    })

    def __init__(self, *, hyperparams: Hyperparams, random_seed: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        self._random: random.Random = random.Random()
        self._random.seed(random_seed)
        self._training_outputs: Outputs = None
        self._fitted = False
        self._classes: typing.List = []

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_outputs = outputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_outputs is None:
            raise exceptions.InvalidStateError("Missing training data.")

        if isinstance(self._training_outputs, container.DataFrame):
            self._classes = self._training_outputs.iloc[:, 0].unique().tolist()
        elif isinstance(self._training_outputs, container.List):
            self._classes = list(set(self._training_outputs))
        elif isinstance(self._training_outputs, container.ndarray):
            self._classes = list(set(self._training_outputs[:, 0]))
        else:
            raise exceptions.InvalidArgumentTypeError('Invalid training outputs type: {type}'.format(
                type=type(self._training_outputs),
            ))

        self._fitted = True

        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if not self._fitted:
            raise exceptions.PrimitiveNotFittedError("Not fitted.")

        k = len(inputs)
        predictions = self._random.choices(self._classes, k=k)  # type: ignore

        if isinstance(inputs, container.DataFrame):
            result = container.DataFrame({'predictions': predictions}, generate_metadata=True)
        elif isinstance(inputs, container.List):
            result = container.List(predictions, generate_metadata=True)
        elif isinstance(inputs, container.ndarray):
            result = container.ndarray(predictions).reshape((k, 1))
            result.metadata = result.metadata.generate(result)
        else:
            raise exceptions.InvalidArgumentTypeError('Invalid training outputs type: {type}'.format(
                type=type(self._training_outputs),
            ))

        return CallResult(result)

    def get_params(self) -> Params:
        if self._fitted:
            return Params(
                classes=self._classes,
            )
        else:
            return Params(
                classes=None,
            )

    def set_params(self, *, params: Params) -> None:
        self._classes = params['classes']
        if self._classes is not None:
            self._fitted = True
